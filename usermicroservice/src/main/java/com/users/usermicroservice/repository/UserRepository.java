package com.users.usermicroservice.repository;

import com.users.usermicroservice.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface UserRepository extends JpaRepository<Users, String> {

    Optional<Users> findByEmailAndPasswordAndType(String email, String password, String type);

    Optional<Users> findByEmail(String email);

    // Update user using @Query and @Modifying
    @Modifying
    @Transactional
    @Query("UPDATE Users u SET u.email = :email, u.name = :name, u.password = :password, u.type = :type WHERE u.userId = :userId")
    boolean updateUser(@Param("userId") String userId,
                       @Param("email") String email,
                       @Param("name") String name,
                       @Param("password") String password,
                       @Param("type") String type);
}
