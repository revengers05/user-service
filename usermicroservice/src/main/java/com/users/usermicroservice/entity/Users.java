package com.users.usermicroservice.entity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.EqualsAndHashCode;
@Entity
@Table(name = "users")
@EqualsAndHashCode
public class Users {
  @Column(name = "userId")
  @Id
  private String userId;

  @Column(name = "email")
  private String email;

  @Column(name = "name")
  private String name;

  @Column(name = "password")
  private String password;

  @Column(name = "type")
  private String type;

  public Users(){}
  public Users(String userId,String email,String name,String password,String type){
      this.userId=userId;
      this.email=email;
      this.name=name;
      this.password=password;
      this.type=type;
  }
  public void setEmpId(String empId) {
    this.userId = userId;
  }

  public String getEmpId() {
    return userId;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
    return userId;
  }
}
