package com.users.usermicroservice.controller;

import com.users.usermicroservice.entity.Users;
import com.users.usermicroservice.model.UserModel;
import com.users.usermicroservice.service.UserServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/v1.0/users")
public class UserController {

    @Autowired
    UserServiceImpl userService;

    @GetMapping("/getDetails/{userId}")
    public UserModel userDetails(@PathVariable String userId) {
        return userService.userDetails(userId);
    }

    @PostMapping("/login")
    public String login(@RequestBody UserModel userModel) {
        boolean res = userService.verify(userModel.getEmail(), userModel.getPassword(), userModel.getType());
        if (res)
            return "success";
        else return "failure";
    }

    @PostMapping("/updateUser")
    public boolean updateUser( @RequestBody UserModel userModel) {
        return userService.updateUser(userModel);
    }

    @PostMapping("/signup")
    public String createUser(@RequestBody UserModel userModel) {
        boolean res = userService.create(userModel);
        if (res)
            return "User Created";
        else
            return "User with the given email already exists";
    }

    @PutMapping("/updatepassword")
    public String updatePassword( @RequestBody UserModel userModel) {
        boolean res = userService.updatePassword( userModel);
        if (res)
            return "Password Updated";
        else
            return "Couldn't update password";
    }
}
