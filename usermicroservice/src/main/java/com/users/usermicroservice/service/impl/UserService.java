package com.users.usermicroservice.service.impl;

import com.users.usermicroservice.entity.Users;
import com.users.usermicroservice.model.UserModel;

import java.security.SecureRandom;
import java.util.Optional;

public interface UserService {
  UserModel userDetails(String userId);

  boolean verify(String email, String password, String type);

  boolean updateUser(UserModel userModel);

  boolean create(UserModel userModel);

  boolean updatePassword(UserModel userModel);
}
