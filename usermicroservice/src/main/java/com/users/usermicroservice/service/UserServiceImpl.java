package com.users.usermicroservice.service;

import com.users.usermicroservice.entity.Users;
import com.users.usermicroservice.model.UserModel;
import com.users.usermicroservice.repository.UserRepository;
import com.users.usermicroservice.service.impl.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

  @Autowired private UserRepository userRepository;

  @Override
  public UserModel userDetails(String userId) {
    Optional<Users> users = userRepository.findById(userId);
    if (users.isPresent()) {
      UserModel userModel =
          new UserModel(
              users.get().getUserId(),
              users.get().getEmail(),
              users.get().getName(),
              users.get().getPassword(),
              users.get().getType());
      return userModel;
    }
    return null;
  }

  @Override
  public boolean verify(String email, String password, String type) {
    Optional<Users> optionalUser =
        userRepository.findByEmailAndPasswordAndType(email, password, type);
    return optionalUser.isPresent();
  }

  @Override
  public boolean updateUser(UserModel userModel) {
    try {
      Optional<Users> optionalUser = userRepository.findById(userModel.getUserId());
      if (optionalUser.isPresent()) {
        Users user = optionalUser.get();
        user.setEmail(userModel.getEmail());
        user.setName(userModel.getName());
        user.setPassword((userModel.getPassword())); // Hash the password
        user.setType(userModel.getType());
        userRepository.save(user);
        return true;
      }
      return false;
    } catch (Exception e) {
      System.out.println(e);
      return false;
    }
  }

  @Override
  public boolean create(UserModel userModel) {
    try {
      if (userRepository.findByEmail(userModel.getEmail()).isPresent()) {
        // User with the given email already exists
        return false;
      }

      Users user =
          new Users(
              userModel.getUserId(),
              userModel.getEmail(),
              userModel.getName(),
              userModel.getPassword(),
              userModel.getType());
      userRepository.save(user);
      return true;
    } catch (Exception e) {
      log.error("error", e);
      // Handle exceptions if necessary
      return false;
    }
  }

  @Override
  public boolean updatePassword(UserModel userModel) {
    try {
      Optional<Users> optionalUser = userRepository.findById(userModel.getUserId());
      if (optionalUser.isPresent()) {
        Users user = optionalUser.get();
        user.setPassword(userModel.getPassword()); // Hash the new password
        userRepository.save(user);
        return true;
      }
      return false;
    } catch (Exception e) {
      System.out.println(e);
      return false;
    }
  }
}
