package com.users.usermicroservice.model;

public class UserModel {
  private String userId;

  private String email;

  private String name;

  private String password;

  private String type;

  public UserModel(String userId, String email, String name, String password, String type) {
    this.userId = userId;
    this.email = email;
    this.name = name;
    this.password = password;
    this.type = type;
  }

  public void setEmpId(String empId) {
    this.userId = userId;
  }

  public String getEmpId() {
    return userId;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getEmail() {
    return email;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getPassword() {
    return password;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserId() {
    return userId;
  }
}
